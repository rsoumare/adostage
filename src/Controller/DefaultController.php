<?php

namespace App\Controller;
use App\Repository\StageRepository;
use App\Repository\EntrepriseRepository;
use App\Entity\Stage;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="default")
     */
    public function index(StageRepository $stageRepository, EntrepriseRepository $entrepriseRepository)
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'stages' => $stageRepository->findAll(),
            'entreprises' => $entrepriseRepository->findAll(),
        ]);
    }


}
